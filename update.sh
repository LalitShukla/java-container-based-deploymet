#sed -i "s|image: my-docker-image|image: $NEXUS_REGISTRY/$NEXUS_REPOSITORY/$IMAGE_NAME|g" charts/values.yaml
#!/bin/bash

# Define the repository URL and image name
REPO_URL="http://utilities.hcldevopscoe.com:30323"
IMAGE_NAME="my-docker-image"

# Retrieve the latest image tag from the Nexus repository
IMAGE_TAG=$(curl -s "${REPO_URL}/service/rest/v1/search/assets/download" \
    | jq -r '.items[0].properties.tag' \
    | awk -F'-' '{print $NF}')

# Update the values.yaml file with the new image name
sed -i "s|image: .*|image: ${REPO_URL}/docker-repo/${IMAGE_NAME}:${IMAGE_TAG}|" charts/values.yaml


