package com.example.app1;
import java.util.ArrayList;
import java.util.List;

public class Example {
    
    // Repetitive code
    public void doSomething(int x, int y) {
        if (x == 0) {
            System.out.println("x is zero");
        } else if (x == 1) {
            System.out.println("x is one");
        } else if (x == 2) {
            System.out.println("x is two");
        } else {
            System.out.println("x is greater than two");
        }
        
        if (y == 0) {
            System.out.println("y is zero");
        } else if (y == 1) {
            System.out.println("y is one");
        } else if (y == 2) {
            System.out.println("y is two");
        } else {
            System.out.println("y is greater than two");
        }
    }
    
    // Bug
    public void divideByZero() {
        int x = 10;
        int y = 0;
        int z = x / y; // Division by zero will cause an ArithmeticException
        System.out.println(z);
    }
    
    // Memory leak
    public void memoryLeak() {
        List<String> list = new ArrayList<>();
        while (true) {
            list.add("a"); // The list will continue to grow indefinitely
        }
    }
    
    // Vulnerability
    public void sqlInjection(String username) {
        String query = "SELECT * FROM users WHERE username='" + username + "'";
        // This code is vulnerable to SQL injection attacks
        // An attacker could pass in a malicious string for the username parameter
        // that could modify the query or retrieve sensitive data
        System.out.println(query);
    }
}
