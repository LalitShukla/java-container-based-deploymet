#!/bin/bash

QGSTATUS=$(curl -u 7ef1735747ea876b9f7427057dddc12d40e2b184: "http://15.206.157.46:9000/api/qualitygates/project_status?projectKey=my-app")

echo $QGSTATUS

if [[ $QGSTATUS = *"\"status\":\"ERROR\""* ]]; then
  echo "Build breaker"
  exit 1
else
  touch ./codescan_succeeded
  exit 0
fi
